var express = require('express');
var router = express.Router();
var memberController = require('../controllers/memberController')
/* GET home page. */
router.post('/add',memberController.addMember);
router.post('/delete',memberController.deleteMember);

module.exports = router;

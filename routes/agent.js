var express = require('express');
var router = express.Router();
var agentController = require('../controllers/agentController')
/* GET home page. */
router.post('/add',agentController.addAgent);
router.post('/delete',agentController.deleteAgent);

module.exports = router;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var agent = new Schema({
    nationalId: {
        type: String,
    },
    photo1: {
        type: String
    },
    photo2: {
        type: String
    },
    name: {
        type: String
    },
    mobileNo: {
        type: String
    },
    creationDate: {
        type: Date
    },
    Adress: {
        type: String
    },
});

module.exports = mongoose.model('agent', agent);
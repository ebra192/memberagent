const Agent = require('../models/agent');
const Member = require('../models/member');
const mongoose = require('mongoose');

module.exports.addMember = async function (req , res){

    try {
        

        if(!req.body.agentId) {
        
            return res.status(400).json({status : 'BAD_REQUEST', message : "agent id is required"});

        }

        let agent = Agent.findById(mongoose.Types.ObjectId(req.body.agentId))

        if(!agent){
         
            return res.status(400).json({status : 'BAD_REQUEST', message : "invalid agent id"});

        }

        let member = new Member( {...req.body , creationDate : new Date()});
        await member.save();
        return res.status(200).json({status : 'OK', result : member});


    } catch (error) {

        console.log(error);
        return res.status(500).json("SERVER_ERROR");     
    }
}

module.exports.deleteMember = async function(req , res) {


    try {
        
        await Member.findByIdAndRemove(mongoose.Types.ObjectId(req.body.id))
        return res.status(200).json({status : 'OK'});

    } catch (error) {

         console.log(error);
         return res.status(500).json("SERVER_ERROR");        
    }

}
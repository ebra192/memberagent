const Agent = require('../models/agent');
const Member = require('../models/member');
const mongoose = require('mongoose');

module.exports.addAgent = async function (req , res) {

    try {
        
        let agent = new Agent( {...req.body , creationDate : new Date()});
        await agent.save();
        return res.status(200).json({status : 'OK', result : agent});

    } catch (error) {

         console.log(error);
         return res.status(500).json("SERVER_ERROR");        
    }
}

module.exports.deleteAgent = async function(req , res) {


    try {
        
        await Agent.findByIdAndRemove(mongoose.Types.ObjectId(req.body.id));
        await Member.remove({agentId : mongoose.Types.ObjectId(req.body.id)})
        return res.status(200).json({status : 'OK', result : agent});

    } catch (error) {

         console.log(error);
         return res.status(500).json("SERVER_ERROR");        
    }

}